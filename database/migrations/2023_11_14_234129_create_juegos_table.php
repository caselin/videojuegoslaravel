<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('juegos', function (Blueprint $table) {
            $table->id();
            $table->string('titulo',100);
            $table->string('empresa',50);
            $table->foreignId('id_genero')->constrained('generos')->onUpdate('cascade')->onDelete('restrict');
            $table->string('descripcion',100);
            $table->integer('precio');
            $table->date('fecha');
            $table->string('pais',50);
            
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('juegos');
    }
};
