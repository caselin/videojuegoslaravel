<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Generos;

class GenerosController extends Controller
{
    
    public function index()
    {
        $generos = Generos::all();
        return view('generos', compact('generos'));
    }

    
    public function create()
    {
        
    }

    
    public function store(Request $request)
    {
        $genero = new Generos($request->input());
        $genero->saveOrFail();
        return redirect('generos');
    }

    public function show(string $id)
    {
        $genero = Generos::find($id);
        return view('editGenero', compact('genero'));
    }

    
    public function edit(string $id)
    {
        
    }

    
    public function update(Request $request, string $id)
    {
        $genero = Generos::find($id);
        $genero->fill($request->input())->saveOrFail();
        return redirect('generos');
    }

    public function destroy(string $id)
    {
        $genero = Generos::find($id);
        $genero->delete(); 
        return redirect('generos');
    }
}
