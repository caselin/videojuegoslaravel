<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Juegos;
use App\Models\Generos;

class JuegosController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $juegos = Juegos::select('juegos.id','titulo','empresa','id_genero','genero','descripcion','precio','fecha','pais')
        ->join('generos','generos.id','=','juegos.id_genero')->get();
        $generos = Generos::all();
        return view('juegos', compact('juegos','generos'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $juego = new Juegos($request->input());
        $juego->saveOrFail();
        return redirect('juegos');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $juego = Juegos::find($id);
        $generos = Generos::all();
        return view('editJuego', compact('juego','generos'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $juego = Juegos::find($id);
        $juego->fill($request->input())->saveOrFail();
        return redirect('juegos');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $juego = juegos::find($id);
        $juego->delete(); 
        return redirect('juegos');
    }
}
