<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Juegos extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable = ['titulo','empresa','id_genero','descripcion','precio','fecha','pais'];
}
