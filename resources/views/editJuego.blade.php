@extends('plantilla')
@section('contenido')
    <div class="row mt-3">
        <br>
        <br>
        <br>
        <div class="col-md-6 offset-md-3">
            <div class="card-header bg-dark text-white">Editar Juego</div>
            <div class="card-body">
                <form id="frmGeneros" method="POST" action="{{url("juegos",[$juego])}}">
                    @method('PUT')
                    @csrf
                    <div class="input-group mb-3">
                        <span class="input-group-text"><i class="fa-solid fa-trophy"></i></span>
                        <input type="text" name="titulo" value="{{ $juego->titulo }}" class="form-control" maxlength="50" placeholder="Titulo" required>
                    </div>
                    <div class="input-group mb-3">
                        <span class="input-group-text"><i class="fa-solid fa-building"></i></span>
                        <input type="text" name="empresa" value="{{ $juego->empresa }}" class="form-control" maxlength="50" placeholder="Empresa" required>
                    </div>
                    <div class="input-group mb-3">
                        <span class="input-group-text"><i class="fa-solid fa-gamepad"></i></span>
                        <select name="id_genero" class="form-select" required>
                            <option value="">Genero</option>
                            @foreach($generos as $row)
                                @if ($row->id==$juego->id_genero)
                                    <option selected value="{{ $row->id }}">{{ $row->genero }}</option>
                                @else
                                    <option value="{{ $row->id }}">{{ $row->genero }}</option>
                                @endif

                            @endforeach
                        </select>
                    </div>
                    <div class="input-group mb-3">
                        <span class="input-group-text"><i class="fa-solid fa-pen"></i></span>
                        <input type="text" name="descripcion" value="{{ $juego->descripcion }}" class="form-control" maxlength="50" placeholder="Descripcion" required>
                    </div>
                    <div class="input-group mb-3">
                        <span class="input-group-text"><i class="fa-solid fa-money-bill"></i></span>
                        <input type="number" name="precio" value="{{ $juego->precio }}" class="form-control" maxlength="50" placeholder="Precio de salida" required>
                    </div>
                    <div class="input-group mb-3">
                        <span class="input-group-text"><i class="fa-solid fa-calendar-days"></i></span>
                        <input type="date" name="fecha" value="{{ $juego->fecha }}" class="form-control" maxlength="50" placeholder="Fecha de salida" required>
                    </div>
                    <div class="input-group mb-3">
                        <span class="input-group-text"><i class="fa-solid fa-earth-americas"></i></span>
                        <input type="text" name="pais" value="{{ $juego->pais }}" class="form-control" maxlength="50" placeholder="Pais de origen" required>
                    </div>

                    <div class="d-grid col-6 mx-auto">
                        <button class="btn btn-success"><i class="fa-solid fa-floppy-disk"></i>  Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection