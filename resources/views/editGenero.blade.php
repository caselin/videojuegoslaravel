@extends('plantilla')
@section('contenido')
    
    <div class="row mt-3 ">
        <br>
        <br>
        <br>
        <div class="col-md-6 offset-md-3">
            <div class="card-header bg-dark text-white">Editar Genero</div>
            <div class="card-body">
                <form id="frmGeneros" method="POST" action="{{url("generos",[$genero])}}">
                    @method('PUT')
                    @csrf
                    <div class="input-group mb-3">
                        <span class="input-group-text"><i class="fa-solid fa-gamepad"></i></span>
                        <input type="text" name="genero"  value="{{ $genero->genero }}"  class="form-control" maxlength="50" placeholder="Genero" required>
                    </div>
                    <div class="d-grid col-6 mx-auto">
                        <button class="btn btn-success"><i class="fa-solid fa-floppy-disk"></i>  Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection