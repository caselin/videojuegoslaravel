@extends('plantilla')
@section('contenido')
    <div class="row mt-3">
        <div class="col-md-4 offset-md-4">
            <div class="d-grid mx-auto">
                <br>
                <br>
                <br>
                <button class="btn btn-dark" data-bs-toggle="modal" data-bs-target="#modalGeneros">
                    <i class="fa-solid fa-circle-plus"></i> Añadir
                </button>
            </div>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-12 col-lg-8 offset-0 offset-lg-2">
            <div class="table-responsive">
                <table class="table table-bordered  bg-dark text-light">
                    <thead>
                        <tr>
                            <th><i class="fa-solid fa-gamepad"></i></th>
                            <th>Titulo</th>
                            <th>Empresa</th>
                            <th>Genero</th>
                            <th>Descripcion</th>
                            <th>Precio de salida</th>
                            <th>Fecha de Salida</th>
                            <th>Pais de origen</th>
                            <th>Editar</th>
                            <th>Eliminar</th>
                        </tr>
                    </thead>
                    <tbody class="table-group-divider">
                        @php $i=1; @endphp
                        @foreach ($juegos as $row)
                            <tr>
                                <td>{{ $i++ }}</td>
                                <td>{{ $row->titulo }}</td>
                                <td>{{ $row->empresa }}</td>
                                <td>{{ $row->genero }}</td>
                                <td>{{ $row->descripcion }}</td>
                                <td>{{ $row->precio }}</td>
                                <td>{{ $row->fecha }}</td>
                                <td>{{ $row->pais }}</td>
                                <td>
                                    <a href="{{ url('juegos',[$row]) }}" class="btn btn-success"><i class="fa-solid fa-edit"></i></a>
                                </td>
                                <td>
                                    <form method="POST" action="{{ url('juegos',[$row]) }}">
                                        @method("delete")
                                        @csrf
                                        <button class="btn btn-danger"><i class="fa-solid fa-trash"></i></button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalGeneros" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <label class="h5" id="titulo_modal">Añadir Juego</label>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form id="frmGeneros" method="POST" action="{{url("juegos")}}">
                        @csrf
                        <div class="input-group mb-3">
                            <span class="input-group-text"><i class="fa-solid fa-trophy"></i></span>
                            <input type="text" name="titulo" class="form-control" maxlength="50" placeholder="Titulo" required>
                        </div>
                        <div class="input-group mb-3">
                            <span class="input-group-text"><i class="fa-solid fa-building"></i></span>
                            <input type="text" name="empresa" class="form-control" maxlength="50" placeholder="Empresa" required>
                        </div>
                        <div class="input-group mb-3">
                            <span class="input-group-text"><i class="fa-solid fa-gamepad"></i></span>
                            <select name="id_genero" class="form-select" required>
                                <option value="">Genero</option>
                                @foreach($generos as $row)
                                <option value="{{ $row->id }}">{{ $row->genero }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="input-group mb-3">
                            <span class="input-group-text"><i class="fa-solid fa-pen"></i></span>
                            <input type="text" name="descripcion" class="form-control" maxlength="50" placeholder="Descripcion" required>
                        </div>
                        <div class="input-group mb-3">
                            <span class="input-group-text"><i class="fa-solid fa-money-bill"></i></span>
                            <input type="number" name="precio" class="form-control" maxlength="50" placeholder="Precio de salida" required>
                        </div>
                        <div class="input-group mb-3">
                            <span class="input-group-text"><i class="fa-solid fa-calendar-days"></i></span>
                            <input type="date" name="fecha" class="form-control" maxlength="50" placeholder="Fecha de salida" required>
                        </div>
                        <div class="input-group mb-3">
                            <span class="input-group-text"><i class="fa-solid fa-earth-americas"></i></span>
                            <input type="text" name="pais" class="form-control" maxlength="50" placeholder="Pais de origen" required>
                        </div>

                        <div class="d-grid col-6 mx-auto">
                            <button class="btn btn-success"><i class="fa-solid fa-floppy-disk"></i>  Guardar</button>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" id="btnCerrar" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
@endsection